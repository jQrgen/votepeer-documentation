# Firebase schema for backend

Implementation details for database schema in the backend. The backend uses
[firebase](https://firebase.google.com/) to simplify mobile support, including
push notifications.

# Collection: election

The collection `election` contains an entry for every election

## Common fields

Fields common for all contract types

*   adminAddress - Bitcoin address of the user who created the election
*   createdAt - The time the database entry was created
*   description - Election description
*   endHeight - The height of last block this election can be voted on (inclusive)
*   modifiedAt - The time the database entry was last modified
*   salt - The unique random nonce for this election
*   contractType - String identifying what contract this election uses
*   option - String list of options users can vote for
*   tags - String list of custom tags used for filtering

## 'Two option vote' fields

Fields used in two-option-vote election (in addition to common fields).

This contract is identified with the string "twoptionvote" in the
`contractType` field.

*   participantAddresses - List of participants, identified with Bitcoin Cash addresses.

## 'Ring signature' fields

Fields used in ring signature election (in addition to common fields).

This contract is identified with the string "ringsignature" in the
`contractType` field.

*   beginHeight - The block height at which the election can be first voted on (inclusive)
*   androidNotificationToken
*   link\_ringsig\_message - A message that was signed to link ring signature public key.
*   link\_ringsig\_proof - The signature signing `link_ringsig_message` with `link_ringsig_pubkey`
*   link\_ringsig\_pubkey - The ring signature public key user has linked to their account.

## Collection: group

A group of user that someone has created.

*   title - Title of this group
*   owner - User identifier (BCH address) of the groups owner.
*   participants - List of users (BCH addresses) belonging to group.

## Collection: login

Temporary data used during authentication. See also <authentication> documentation.

*   authToken - Authentication token we provide to user when they successfully
    solve a challenge.

*   challenge - Random data we challenged a user to sign to prove ownership of
    his identity (BCH address)

## Collection: user

*   allowedTags - Tags that a user is allowed to assign to an election (aka whitelist)
